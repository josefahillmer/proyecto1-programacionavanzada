#!/usr/bin/env python3
 # -*- coding:utf-8 -*-

import pandas as pd
from Aplicacion import Aplicacion
from Peliculas import Titulo

def archivo():
    archivo = pd.read_csv("IMDb movies.csv")
    return archivo

def crear_titulos(pelis_chilenas):
    titulos = []
    for i in pelis_chilenas:
        obj_titulos = Aplicacion()
        obj_titulos.set_titulo(titulos)
        titulos.append(obj_titulos)
    print(titulos)
    return titulos


def titulos(pelis_chilenas, titulos):
    print(type(pelis_chilenas))
    print(type(titulos))
    """
    for index, row in pelis_chilenas.interrows():
        titulos = row["title"].capitalize()
        print(titulos)
    """

if __name__ == "__main__":
    archivo = archivo()
    #print(archivo.columns)
    # Se crea un Data de que queremos y de esat forma filtrar el arhivo original
    new = archivo.filter(["title", "year", "country", "reviews_from_users"])
    #print(new)
    try:
        print("Bienvenido a la aplicacion de peliculas chilenas")
        print("Ingrese una opcion: ")
        opcion = int(input("1. Ver listado de peliculas: "))
        if opcion == 1:
            # Para seleccionar solo las peliculas que son chilenas
            pelis_chilenas = new[new["country"]=="Chile"]
            print("En la biblioteca hay {} peliculas chilenas".format(len(pelis_chilenas)))
            #print(pelis_chilenas)
            # Para crear un archivo solo con los datos que vamos a usar
            #pelis_chilenas.to_csv("Peliculas-chilenas.csv", sep=';', index=False)
            print(pelis_chilenas)
            pelis = crear_titulos(pelis_chilenas)
            pelis = titulos(pelis_chilenas, pelis)

    except ValueError:
        print("Opcion invalida")
