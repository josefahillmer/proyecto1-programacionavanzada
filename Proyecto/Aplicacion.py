#!/usr/bin/env python3
 # -*- coding:utf-8 -*-

from Peliculas import Titulo

class Aplicacion():
    def __init__(self):
        self.__titulo = None

    def get_titulo(self):
        return self.__titulo

    def set_titulo(self, titulo):
        if isinstance(titulo, str):
            self.__titulo = titulo
